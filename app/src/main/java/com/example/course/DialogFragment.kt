package com.example.course

import android.annotation.SuppressLint
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.FileUtils
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.course.adapter.MessageAdapter
import com.example.course.data.Query
import com.example.course.databinding.FragmentDialogBinding
import com.example.course.models.DataViewModel
import com.example.course.models.Message
import okhttp3.MediaType
import okhttp3.RequestBody
import org.intellij.lang.annotations.RegExp
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.io.File
import java.io.FileInputStream
import kotlin.properties.Delegates


class DialogFragment : Fragment() {
    private lateinit var binding:FragmentDialogBinding
    private val mainActivity:MainActivity = MainActivity()
    private val dataViewModel:DataViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDialogBinding.inflate(inflater)
        return binding.root
    }


    @SuppressLint("NotifyDataSetChanged", "SetTextI18n", "ResourceType", "Recycle")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        var messageList:MutableList<Message> = mutableListOf(Message("Привет","12:34",true),
            Message("Привет","12:34",false),
            Message("Как дела","12:35",true),
            Message("Все хорошо, у тебя как? У меня столько всего нового!!","12:36",false),
            Message("Здорово","12:34",true),
            Message("Все хорошо, у тебя как? У меня столько всего нового!!","12:36",true))
        var mes:MutableList<Message> = mutableListOf(Message("Привет","12:34",true),
            Message("Привет","12:34",false),
            Message("Как дела","12:35",true))
        messageList.reverse()
        mes.reverse()
        val adapter = MessageAdapter(mes)
        binding.messageRecyclerView.layoutManager = LinearLayoutManager(activity,RecyclerView.VERTICAL,true)
        binding.messageRecyclerView.adapter = adapter


        binding.send.isEnabled = false
        binding.newMessageEditText.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(
                p0: CharSequence?, start: Int,
                count: Int, after: Int,
            ) {
            }

            override fun onTextChanged(
                p0: CharSequence?, start: Int,
                count: Int, after: Int,
            ) {
            }

            override fun afterTextChanged(p0: Editable?) {
                binding.send.isEnabled = true
            }

        })

        binding.send.setOnClickListener{
            if(!binding.newMessageEditText.text.isEmpty()){
                adapter.addMessage(Message(binding.newMessageEditText.text.toString(),"13:50",true))
                binding.newMessageEditText.text.clear()

            }


            binding.send.isEnabled = false

        }
        binding.attachButton.setOnClickListener{
            if(binding.onClickAttach.visibility == View.GONE){
                binding.onClickAttach.visibility = View.VISIBLE
                binding.onClickAttach.animation = AnimationUtils.loadAnimation(activity,R.anim.attach_open_animation)
            }
            else  {
                binding.onClickAttach.visibility = View.GONE
                binding.onClickAttach.animation = AnimationUtils.loadAnimation(activity,R.anim.attach_close_anim)
            }

        }



        val getContent = registerForActivityResult(ActivityResultContracts.GetContent()){


            lateinit var cursor:Cursor

            if (it != null){

                    cursor = activity?.contentResolver?.query(it,null,null,null,null)!!
                val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)

                cursor.moveToFirst()
                    println(cursor.getString(nameIndex))
                    var path:String = ""
                    var pathArray = it.path?.split("/")?.toMutableList()
                    pathArray?.set(pathArray.size - 1, cursor.getString(nameIndex))
                if (pathArray != null) {
                    for (el in pathArray){

                        path += if(el == pathArray[pathArray.lastIndex]) el
                                else "$el/"

                    }
                }
                    println(path)
                    var image:File = File(it.path)
                    println(image.name)
                var fin:FileInputStream = FileInputStream(image)

                    val url = "http://192.168.56.1:3000/"

                    val retrofit = Retrofit.Builder()
                        .baseUrl(url)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                    val service = retrofit.create(Query::class.java)

                    var reqData:RequestBody = RequestBody.create(MediaType.get("multipart/form-data"),image)

                    val res = service.sendFile(reqData)

                    res.enqueue(object :Callback<String>{
                        override fun onResponse(call: Call<String>, response: Response<String>) {
                            println(response.body())
                            Toast.makeText(activity,"Открыли  ",Toast.LENGTH_SHORT).show()
                        }

                        override fun onFailure(call: Call<String>, t: Throwable) {
                            println("Chet ne tak")
                            t.printStackTrace()
                            Toast.makeText(activity,"не вышло  ",Toast.LENGTH_SHORT).show()
                        }

                    })
                    println(res.toString())


                }

        }
        binding.documentAttach.setOnClickListener {


            getContent.launch("*/*")


        }
        binding.imageAttach.setOnClickListener {
            getContent.launch("image/*")

        }

        dataViewModel.employee.observe(activity as LifecycleOwner,{
            binding.dialogFragmentFio.text = it.surname + " " + it.firstname + " " + it.patronymic
        })
    }

    companion object {

        @JvmStatic
        fun newInstance() = DialogFragment()

    }

}
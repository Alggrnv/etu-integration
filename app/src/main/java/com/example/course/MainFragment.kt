package com.example.course

import android.animation.StateListAnimator
import android.annotation.SuppressLint
import android.content.res.Resources
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.course.databinding.FragmentMainBinding
import com.example.course.models.DataViewModel


class MainFragment : Fragment() {


    private lateinit var binding: FragmentMainBinding
    private val dataViewModel:DataViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainBinding.inflate(inflater)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility", "UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewModel.toolbarTitle.value = arguments?.getString("title")


        binding.allMessageContainer.setOnClickListener {
            findNavController().navigate(R.id.allMessageFragment)
        }

        binding.divisionsContainer.setOnClickListener {
            findNavController().navigate(R.id.divisionsFragment)
        }

        binding.notebookContainer.setOnClickListener {
            findNavController().navigate(R.id.notebookFragment)

        }



    }
    @SuppressLint("UseCompatLoadingForDrawables")
//
    companion object {
        @JvmStatic
        fun newInstance() = MainFragment()

    }
}






//        binding.allMessageContainer.setOnTouchListener { _, motion ->
//            when (motion?.action) {
//                MotionEvent.ACTION_DOWN -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element_onclick,
//                        R.drawable.ic_mail_white)
//                }
//                MotionEvent.ACTION_MOVE -> isMove = true
//                MotionEvent.ACTION_UP -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element,
//                        R.drawable.ic_mail,false)
//                    if (!isMove) findNavController().navigate(R.id.allMessageFragment)
//                    isMove = false
//                }
//            }
//            true
//        }
//        binding.allMessageContainer.setOnTouchListener { _, motion ->
//            when (motion?.action) {
//                MotionEvent.ACTION_DOWN -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element_onclick,
//                        R.drawable.ic_mail_white)
//                }
//                MotionEvent.ACTION_MOVE -> isMove = true
//                MotionEvent.ACTION_UP -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element,
//                        R.drawable.ic_mail,false)
//                    if (!isMove) findNavController().navigate(R.id.allMessageFragment)
//                    isMove = false
//                }
//            }
//            true
//        }
//        binding.allMessageContainer.setOnTouchListener { _, motion ->
//            when (motion?.action) {
//                MotionEvent.ACTION_DOWN -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element_onclick,
//                        R.drawable.ic_mail_white)
//                }
//                MotionEvent.ACTION_MOVE -> isMove = true
//                MotionEvent.ACTION_UP -> {
//                    whenCase(binding.allMessageContainer,
//                        binding.allMessageTitle,
//                        binding.allMessageTitleDescription,
//                        binding.imageView,
//                        R.drawable.menu_element,
//                        R.drawable.ic_mail,false)
//                    if (!isMove) findNavController().navigate(R.id.allMessageFragment)
//                    isMove = false
//                }
//            }
//            true
//        }

//private fun whenCase(container:ConstraintLayout,
//                        title:TextView,
//                        titleDescription:TextView,
//                        imageView: ImageView, background: Int, icon:Int,isDown:Boolean = true){
//        container.background = activity?.getDrawable(background)
//        imageView.setImageResource(icon)
//        if (isDown){
//            title.setTextColor(resources.getColor(R.color.white))
//            titleDescription.setTextColor(resources.getColor(R.color.white_whith_opasity))
//        } else {
//            title.setTextColor(resources.getColor(R.color.etu_blue))
//            titleDescription.setTextColor(resources.getColor(R.color.etu_blue_description))
//        }
//
//
//
//    }
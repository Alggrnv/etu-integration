package com.example.course.models

import android.text.Spanned

class Employee(var surname:String,
               var firstname:String,
               var patronymic:String,
               var merits:List<String>) {

    fun getFio() : Spanned {
        return android.text.Html.fromHtml("<u>"+
                surname + " " +
                firstname + " " +
                patronymic + "</u>")
    }
}
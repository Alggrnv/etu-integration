package com.example.course.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataViewModel: ViewModel() {
    val employee:MutableLiveData<Employee> by lazy {
        MutableLiveData<Employee>()
    }
    val toolbarTitle:MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}
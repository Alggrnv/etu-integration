package com.example.course

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.course.adapter.EmployeeAdapter
import com.example.course.databinding.FragmentDivisionsBinding
import com.example.course.models.Employee
import com.example.course.models.DataViewModel


class DivisionsFragment : Fragment() {


    private lateinit var binding: FragmentDivisionsBinding
    private val dataViewModel:DataViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDivisionsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewModel.toolbarTitle.value = arguments?.getString("title")
        var meritsArray = listOf<String>("специалист по учебно-методической работе 1 категории дек.",
            "старший научный сотрудник кандидат наук МНИИ",
            "старший преподаватель каф.АМ","профессор доктор наук каф.ВТ","начальник отдела ОРПК")
        var employees: MutableList<Employee> =
            mutableListOf(Employee("Овсянникова", "Марьям", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray))


        val employeeLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        employeeLayoutParams.setMargins(35,35,35,0)






        val adapter = activity?.let { EmployeeAdapter(it,employees,dataViewModel,findNavController()) }

        binding.employeeList.adapter = adapter




    }

    companion object {
        @JvmStatic
        fun newInstance() = DivisionsFragment()
    }


}
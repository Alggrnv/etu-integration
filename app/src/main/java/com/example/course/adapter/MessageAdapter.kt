package com.example.course.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.course.R
import com.example.course.databinding.MessageBinding
import com.example.course.models.Message

class MessageAdapter(private var messages:MutableList<Message>):RecyclerView.Adapter<MessageAdapter.MessageHolder>() {
    lateinit var recyclerView: RecyclerView
    inner class MessageHolder(item:View):RecyclerView.ViewHolder(item){
        private val binding = MessageBinding.bind(item)
        fun bind(message:Message){
            binding.messageBody.text = message.messageBody
            binding.time.text = message.time
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {

        recyclerView = parent.findViewById(R.id.messageRecyclerView)

        return if(viewType == 1) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.message_right,parent,false)
            MessageHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.message,parent,false)
            MessageHolder(view)
        }


    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        holder.bind(messages[position])

    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].isMeMessage) 1 else 0
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addMessage(message: Message){
        messages.add(0,message)
        notifyItemInserted(0)
        recyclerView.smoothScrollToPosition(0)

    }
}
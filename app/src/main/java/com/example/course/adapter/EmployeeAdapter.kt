package com.example.course.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavController
import com.example.course.R
import com.example.course.models.Employee
import com.example.course.models.DataViewModel

class EmployeeAdapter(private val context:Context,
                      private val employees:List<Employee>,
                      private val dataViewModel: DataViewModel,
                      private val findNavController: NavController ):BaseAdapter() {
    private val inflater:LayoutInflater = LayoutInflater.from(context)
    override fun getCount(): Int {
        return employees.size
    }

    override fun getItem(position: Int): Any {
        return employees[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder", "UseCompatLoadingForDrawables", "SetTextI18n")
    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val itemView = inflater.inflate(R.layout.employee,parent,false)

        val fioTextView:TextView = itemView.findViewById(R.id.fioTextView)
        val meritsLinearLayout: LinearLayout = itemView.findViewById(R.id.meritsLinearLayout)

        val dp:Float = context.resources.displayMetrics.density



        val meritLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        meritLayoutParams.setMargins((23*dp).toInt(),(5*dp).toInt(),(20*dp).toInt(),0)

        itemView.setOnClickListener{
            val child:ConstraintLayout = itemView.findViewById(R.id.employeeChild)
            val fioTextView:TextView = itemView.findViewById(R.id.fioTextView)

            fioTextView.setTextColor(Color.WHITE)
            child.background = context.getDrawable(R.drawable.employee_element_onclick)
            dataViewModel.employee.value = employees[position]
            findNavController.navigate(R.id.navDialogFragment)

        }

        fioTextView.text = employees[position].getFio()
        for((i,meritItem) in employees[position].merits.withIndex()){

            val merit:TextView = TextView(context)
            merit.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)
            merit.setTextColor(Color.parseColor("#979797"))
            merit.layoutParams = meritLayoutParams
            merit.text = meritItem
            meritsLinearLayout.addView(merit)

        }
        return itemView
    }


}
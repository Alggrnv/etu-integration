package com.example.course.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavController
import com.example.course.R
import com.example.course.models.Dialog
import com.example.course.models.Employee
import com.example.course.models.DataViewModel

class DialogAdapter(private val context:Context,
                    private var dialogs:List<Dialog>,
                    private val dataViewModel:DataViewModel,
                    private val findNavController: NavController
):BaseAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return dialogs.size
    }

    override fun getItem(position: Int): Any {
        return dialogs[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val itemView = inflater.inflate(R.layout.dialog,parent,false)

        val messageFrom:TextView = itemView.findViewById(R.id.messageFrom)
        val merit:TextView = itemView.findViewById(R.id.merit)
        val date:TextView = itemView.findViewById(R.id.date)

        messageFrom.text = dialogs[position].surname + " " +
                dialogs[position].firstname + " " +
                dialogs[position].patronymic
        merit.text = dialogs[position].merit
        date.text = dialogs[position].date
        val child:ConstraintLayout = itemView.findViewById(R.id.messageChild)
        itemView.setOnClickListener{
            dataViewModel.employee.value = Employee(dialogs[position].surname,
                dialogs[position].firstname,
                dialogs[position].patronymic, listOf(""))
            child.background = context.getDrawable(R.drawable.employee_element_onclick)
            messageFrom.setTextColor(context.resources.getColor(R.color.white))
            merit.setTextColor(Color.parseColor("#99FFFFFF"))
            date.setTextColor(Color.parseColor("#CCFFFFFF"))
            findNavController.navigate(R.id.navDialogFragment)
        }


        return itemView
    }
}
package com.example.course

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.course.databinding.FragmentNotebookBinding
import com.example.course.models.DataViewModel
import com.example.course.models.Dialog
import com.example.course.models.Employee


class NotebookFragment : Fragment() {

    private var countChild:Int = 0
    private lateinit var binding:FragmentNotebookBinding
    private val  dataViewMode:DataViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotebookBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewMode.toolbarTitle.value = "Записная книжка"

        binding.button.isEnabled = false

        binding.surnameEditText.addTextChangedListener(ChangeText(binding.surnameEditText))
        binding.firstnameEditText.addTextChangedListener(ChangeText(binding.firstnameEditText))
        binding.patronymicEditText.addTextChangedListener(ChangeText(binding.patronymicEditText))
        var meritsArray = listOf<String>("специалист по учебно-методической работе 1 категории дек.",
            "старший научный сотрудник кандидат наук МНИИ",
            "старший преподаватель каф.АМ","профессор доктор наук каф.ВТ","начальник отдела ОРПК")
        var employees: MutableList<Employee> =
            mutableListOf(Employee("Овсянникова", "Марьям", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Bulet", "Olga", "Vitalich",meritsArray),
                Employee("Ogrov", "Andy", "Popovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray),
                Employee("Ovsyanikova", "Mary", "Davidovich",meritsArray))

        binding.button.setOnClickListener{
            activity?.let { it1 -> addPersonOnView(employees,binding.notebookLayout, it1) }
            var mAct:MainActivity = MainActivity()
            activity?.let { it1 -> mAct.hideKeyboardFrom(it1,view) }
        }

    }
   inner class ChangeText(private val editText: EditText) : TextWatcher{

       override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

       }

       override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

       }

       @SuppressLint("UseCompatLoadingForDrawables")
       override fun afterTextChanged(p0: Editable?) {

           if(editText.text.isNotEmpty()){
               editText.background =
                   activity?.getDrawable(R.drawable.input_with_text)
               binding.button.isEnabled = true
           }

           else {
               editText.background =
                   activity?.getDrawable(R.drawable.input_element)
               binding.button.isEnabled = false

           }
       }

   }
    @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
    private fun addPersonOnView(persons:List<Employee>, linearLayout: LinearLayout,context: Context){

        if(countChild > 0 && binding.notebookLayout.childCount > 7){
            for (i in 0 until countChild){
                var childForDelete:View =
                    binding.notebookLayout.getChildAt(binding.notebookLayout.childCount - 1)
                binding.notebookLayout.removeView(childForDelete)
            }
        }

        countChild = 0

        for (person in persons){


            if(person.surname.contains(binding.surnameEditText.text.toString()) &&
                    person.firstname.contains(binding.firstnameEditText.text.toString()) &&
                    person.patronymic.contains(binding.patronymicEditText.text.toString())){
                countChild++

                val employeeLayout = layoutInflater.inflate(R.layout.employee,null,false)

                val fioTextView:TextView = employeeLayout.findViewById(R.id.fioTextView)

                fioTextView.text = person.getFio()

                employeeLayout.setOnClickListener{
                    val child:ConstraintLayout = employeeLayout.findViewById(R.id.employeeChild)

                    fioTextView.setTextColor(Color.WHITE)
                    child.background = context.getDrawable(R.drawable.employee_element_onclick)
                    dataViewMode.employee.value = person
                    findNavController().navigate(R.id.navDialogFragment)

                }

                val meritsLinearLayout: LinearLayout = employeeLayout.findViewById(R.id.meritsLinearLayout)

                val dp: Float = resources.displayMetrics.density

                val meritLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                meritLayoutParams.setMargins((23* dp).toInt(),(5*dp).toInt(),(20*dp).toInt(),0)

                for(meritItem in person.merits){
                    val merit:TextView = TextView(context)
                    merit.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)
                    merit.setTextColor(Color.parseColor("#979797"))
                    merit.layoutParams = meritLayoutParams
                    merit.text = meritItem
                    meritsLinearLayout.addView(merit)
                }

                linearLayout.addView(employeeLayout)
            }



        }
    }
    companion object {
        @JvmStatic
        fun newInstance() = NotebookFragment() }
}
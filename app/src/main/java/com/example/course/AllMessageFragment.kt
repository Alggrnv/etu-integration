package com.example.course

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.course.adapter.DialogAdapter
import com.example.course.databinding.FragmentAllMessageBinding
import com.example.course.models.Dialog
import com.example.course.models.DataViewModel


class AllMessageFragment : Fragment() {
    private lateinit var binding:FragmentAllMessageBinding

    private val dataViewModel:DataViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAllMessageBinding.inflate(inflater)


        return binding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewModel.toolbarTitle.value = arguments?.getString("title")

        var dialogs = listOf<Dialog>(Dialog("Овсянникова", "Марьям", "Давидовна",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"),Dialog("Белова","Диана", "Демидовна",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"),Dialog("Сухов", "Андрей", "Макарович",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"),Dialog("Голованова", "София", "Тимуровна",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"),Dialog("Овсянникова", "Марьям", "Давидовна",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"),Dialog("Овсянникова", "Марьям", "Давидовна",
            "специалист по учебно-методической работе 1 категории дек.",
            "21.05.2021"))


        val adapter: DialogAdapter? = activity?.let { DialogAdapter(it,dialogs,dataViewModel,findNavController()) }

        binding.messageListView.adapter = adapter
    }
    companion object {
        @JvmStatic
        fun newInstance() = AllMessageFragment()
    }
}
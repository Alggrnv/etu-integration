package com.example.course.data



import okhttp3.RequestBody
import okhttp3.Response
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface Query {
    @Multipart
    @POST(".")
    fun sendFile(
        @Part("file") file:RequestBody
    ) : Call<String>

}
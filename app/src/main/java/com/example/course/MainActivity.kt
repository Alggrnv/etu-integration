package com.example.course

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.course.databinding.ActivityMainBinding
import com.example.course.models.DataViewModel
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.provider.DocumentsContract





class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val dataViewModel:DataViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.menuButton.setOnClickListener(View.OnClickListener {
//            binding.toolbar.menuButton.setImageResource(R.drawable.ic_arrow_on_click)
//            binding.drawerLayout.openDrawer(GravityCompat.START)
            onBackPressed()
            hideKeyboardFrom(this,it)
        })
        setSupportActionBar(binding.toolbar.toolbar)
        dataViewModel.toolbarTitle.observe(this, {
            binding.toolbar.fragmentTitle.text = it
        })


        

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.hostFragment) as NavHostFragment
        navController = navHostFragment.navController
        binding.navView.setupWithNavController(navController)







    }



    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isAcceptingText) imm.hideSoftInputFromWindow(view.windowToken, 0)


    }


}